export interface Image {
    path: string
}

export interface ShowcaseMask {
    id:number,
    title:string,
    price:number,
    images : string,
    description: string
}

