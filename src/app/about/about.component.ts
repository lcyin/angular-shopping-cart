import { Component, OnInit } from '@angular/core';
export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
  img: string;
}
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  
  tiles: Tile[] = [
    {text: 'One', cols: 1, rows: 1, color: 'lightblue', img: 'assets/images/about/one.jpeg'},
    {text: 'Two', cols: 2, rows: 2, color: 'lightgreen', img: 'assets/images/about/two.jpg'},
    {text: 'Three', cols: 1, rows: 1, color: 'lightpink', img: 'assets/images/about/three.jpg'},
    {text: 'Four', cols: 1, rows: 1, color: '#DDBDF1', img: 'assets/images/about/four.jpg'},
    {text: 'Five', cols: 1, rows: 1, color: '#DDBDF1', img: 'assets/images/about/five.jpg'},
    {text: 'Six', cols: 1, rows: 1, color: '#DDBDF1', img: 'assets/images/about/six.jpeg'},
    {text: 'Seven', cols: 2, rows: 2, color: '#DDBDF1', img: 'assets/images/about/seven.jpeg'},
    {text: 'Eight', cols: 1, rows: 2, color: '#DDBDF1', img: 'assets/images/about/eight.jpeg'},
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
