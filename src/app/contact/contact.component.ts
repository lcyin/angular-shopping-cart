 /// <reference types="@types/googlemaps" /> 
 import { Component, AfterViewInit, ViewChild, ElementRef } from 
 '@angular/core';

@Component({
 selector: 'app-contact',
 templateUrl: './contact.component.html',
 styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements AfterViewInit {

 constructor() { }

 title = 'angular-gmap';
 @ViewChild('mapContainer', { static: false } as any) gmap: ElementRef;
 map: google.maps.Map;
 lat = 22.309111;
 lng = 114.189082;

 coordinates = new google.maps.LatLng(this.lat, this.lng);

 mapOptions: google.maps.MapOptions = {
  center: this.coordinates,
  zoom: 18
 };

 marker = new google.maps.Marker({
   position: this.coordinates,
   map: this.map,
 });

 infowindow: google.maps.InfoWindow

 ngAfterViewInit() {
   this.mapInitializer();
 }

 mapInitializer() {
   this.map = new google.maps.Map(this.gmap.nativeElement, 
   this.mapOptions);
   this.marker.setMap(this.map);
   
   this.infowindow = new google.maps.InfoWindow({
     content: ` <a target="__blank" style="color: #b1b1b1;text-decoration: none;" href="https://www.google.com/maps/place/%E5%87%B1%E6%97%8B%E5%B7%A5%E5%95%86%E4%B8%AD%E5%BF%83%E7%AC%AC%E4%BA%8C%E6%9C%9F/@22.3093276,114.1889872,15z/data=!4m5!3m4!1s0x0:0xa958836139279b70!8m2!3d22.3093276!4d114.1889872"> UNIT L8, 4/F., KAISER ESTATE, PHASE II, 47-53 MAN YUE STREET, HUNG HOM, KOWLOON</a>`
   });
   this.infowindow.open(this.map, this.marker);

   this.marker.addListener('click', () => {
     this.infowindow.open(this.map, this.marker)
   });
 }

}
