import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { ShowcaseMask } from 'src/data';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  cartItems = []
  cartTotal:number = 0;

  constructor(
    private productService:ProductService
  ) { }

  ngOnInit(): void {
    this.productService.getCartProduct().subscribe((product:ShowcaseMask) => {
      this.addToCart(product);
      this.countTotal();
    })
  }

  addToCart(product:ShowcaseMask) {
    const item =  this.cartItems.find(item => item.id === product.id )

    if ((this.cartItems.length > 0) && item) {
      item.qty ++
    } else {
      this.cartItems.push({
        id: product.id,
        price: product.price,
        qty:1,
        productName: product.title
      })
    }
  }

  countTotal(){
    let tempTotal = 0
    this.cartItems.forEach(item => {
      tempTotal += (item.price * item.qty)
    });
    this.cartTotal = tempTotal;
  }

  minusItem(item) {
    this.cartItems.forEach(cartItem => {
      if (cartItem.id === item.id && item.qty !== 0) {
        cartItem.qty--
      }
    })
    this.countTotal()
  }

  addItem(item) {
    this.cartItems.forEach(cartItem => {
      if (cartItem.id === item.id) {
        cartItem.qty++
      }
    })
    this.countTotal()
  }
}
