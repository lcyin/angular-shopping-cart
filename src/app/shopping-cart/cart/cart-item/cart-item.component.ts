import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent implements OnInit {

  @Input() cartItem;

  @Output() minusItem = new EventEmitter()
  @Output() addItem = new EventEmitter()


  constructor() { }

  ngOnInit(): void {

  }

  callMinusItem() {
    this.minusItem.emit(this.cartItem)
  }

  callAddItem() {
    this.addItem.emit(this.cartItem)
  }

}
