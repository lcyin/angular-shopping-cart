import { Component, OnInit } from '@angular/core';
import { MatDialog, } from '@angular/material/dialog';
import * as contentful from 'contentful'
import { 
   ShowcaseMask
  } from '../../../data'
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  products:ShowcaseMask[]

  constructor(
    public dialog: MatDialog,
    private productService:ProductService
  ) { }

  async ngOnInit(): Promise<void> {
    this.products = await this.productService.getProducts();

  }

  addToCart(product) {
    this.productService.addToCart(product);
  }

  openDialog(data: ShowcaseMask) {


    // this.dialog.open(ShowcaseGalleryDialogComponent, {
    //   data,
    //   width: '800px',
    //   height: '900px'
    // });
  }


}
