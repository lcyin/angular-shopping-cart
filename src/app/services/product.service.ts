import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import * as contentful from 'contentful'

import { ShowcaseMask } from 'src/data';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  products: ShowcaseMask[]
  subject = new Subject()
  
  constructor() {
   }

    addToCart(product) {
      this.subject.next(product)
    }

    getCartProduct(){
      return this.subject.asObservable()
    }

   async getProducts():Promise<ShowcaseMask[]>{
     const client = contentful.createClient({
      // This is the space ID. A space is like a project folder in Contentful terms
      space: "azv3uidg6r6r",
      // This is the access token for this space. Normally you get both ID and the token in the Contentful web app
      accessToken: "wevFhbiT6xmHxie8TsQL3RLRtB8T8y_B46_PNDjqfIE"
    });
    // This API call will request an entry with the specified ID from the space defined at the top, using a space-specific access token.
     await client
      .getEntries({
        content_type:'maskProducts',
        order: 'sys.createdAt'
      })
      .then(entry => {

        const product :ShowcaseMask[]=[]
        entry.items.forEach((item,index) => {
          const title =( item.fields as any).title
          const price =( item.fields as any).price
          const description =( item.fields as any).description
          const images =( item.fields as any).image.fields.file.url
          product.push({
            id: index + 1,
            title,
            price,
            images,
            description
          })
        })
        this.products = product


      })
      .catch(err => console.log(err));

      return this.products;

   }

}
